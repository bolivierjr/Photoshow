<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AlbumsController extends Controller
{
    public function index()
    {
        return view('albums.index');
    }

    public function create()
    {
        return view('albums.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required',
        'cover_image' => 'image|max:1999'
      ]);

      // Get filename with extension
      $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();

      // Get just the filename
      $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

      // Get extension
      $extension = $request->file('cover_image')->getClientOriginalExtension();

      $filenameToStore = $filename.'_'.time().'.'.$extension;

      // Upload image
      $path = $request->file('cover_image')->storeAs('public/albumcovers', $filenameToStore);

      return $path;
    }
}
